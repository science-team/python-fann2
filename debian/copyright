Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fann2
Source: https://github.com/FutureLinkCorporation/fann2/
Files-Excluded:
 wheels
Comment:
 The source is repackaged to excluded the binary wheels.
 .
 Until FANN-2.1.0, the Python bindings to FANN were provided by src:libfann
 itself. These are now maintained by an external contributor, hence a separate
 source package.

Files: *
Copyright: 2004-2007, Vincenzo Di Massa <hawk@hawk.linuxpratico.net>
           2014-2017, FutureLinkCorporation
License: LGPL-2.1+

Files: include/*
Copyright: 2004-2012, Steffen Nissen <sn@leenissen.dk>
License: LGPL-2.1+

Files: debian/*
Copyright: 2015-2022, Christian Kastner <ckk@debian.org>
License: LGPL-2.1+

Files: debian/patches/*
Copyright: 2015, Christian Kastner <ckk@debian.org>
License: LGPL-2.1+

Files: debian/patches/Add-an-example.patch
Copyright: 2004-2007, Vincenzo Di Massa <hawk@hawk.linuxpratico.net>
           2015-2019, Christian Kastner <ckk@debian.org>
License: LGPL-2.1+

License: LGPL-2.1+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2.1".
